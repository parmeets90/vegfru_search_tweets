package app.vegfrutweets.activities;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import adapters.TweetAdapter;
import app.vegfrutweets.R;
import bean.Tweet;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.OAuth2Token;
import twitter4j.conf.ConfigurationBuilder;
import utils.DialogUtil;

public class MainActivity extends AppCompatActivity {
    private EditText edtSearch;
    private Toolbar toolbar;
    private Button btnSearch;
    private final String TWIT_CONS_KEY = "BE78mg1k37S2pCccuAXQIT9MK";
    private final String TWIT_CONS_SEC_KEY = "FjkphQkOmxOXDd81zPo0hWUj1Pg2LTKn0NSjG97XNA1YtSHy6A";
    private ListView list;
    private boolean isDataVisible;
    private ProgressBar progress;
    private final Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolBar);
        ((TextView) toolbar.findViewById(R.id.title)).setText(R.string.app_name);
        setSupportActionBar(toolbar);
        edtSearch = (EditText) findViewById(R.id.edtSearch);
        btnSearch = (Button) findViewById(R.id.btnSearch);
        list = (ListView) findViewById(R.id.list);
        progress = (ProgressBar) findViewById(R.id.progress);
        doAutoRefresh(isDataVisible);

        edtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.toString().trim().length() == 0) {
                    isDataVisible = false;
                    doAutoRefresh(isDataVisible);
                    list.setVisibility(View.GONE);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnSearch.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (edtSearch.getText().toString().equalsIgnoreCase("")) {
                    edtSearch.setError(getResources().getString(R.string.please_enter));
                } else if (DialogUtil.checkInternetConnection(MainActivity.this)) {
                    new SearchOnTwitter().execute("#" + edtSearch.getText().toString());
                    isDataVisible = true;
                    list.setVisibility(View.VISIBLE);
                    doAutoRefresh(isDataVisible);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edtSearch.getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);
                } else {
                    DialogUtil.showNoConnectionDialog(MainActivity.this);
                }

            }
        });
    }

    private void doAutoRefresh(final boolean isDataVisible) {
        if (isDataVisible) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!edtSearch.getText().toString().equalsIgnoreCase("")) {
                        list.setVisibility(View.VISIBLE);
                        new SearchOnTwitter().execute("#" + edtSearch.getText().toString());
                        doAutoRefresh(isDataVisible);
                    } else {
                        list.setVisibility(View.GONE);
                    }
                }
            }, 20000);
        }
    }

    class SearchOnTwitter extends AsyncTask<String, Void, Integer> {
        ArrayList<Tweet> tweets;
        final int SUCCESS = 0;
        final int FAILURE = SUCCESS + 1;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.setVisibility(View.VISIBLE);
            //dialog.show(MainActivity.this, "", getString(R.string.searching));
        }

        @Override
        protected Integer doInBackground(String... params) {
            try {
                ConfigurationBuilder builder = new ConfigurationBuilder();
                builder.setUseSSL(true);
                builder.setApplicationOnlyAuthEnabled(true);
                builder.setOAuthConsumerKey(TWIT_CONS_KEY);
                builder.setOAuthConsumerSecret(TWIT_CONS_SEC_KEY);

                OAuth2Token token = new TwitterFactory(builder.build()).getInstance().getOAuth2Token();

                builder = new ConfigurationBuilder();
                builder.setUseSSL(true);
                builder.setApplicationOnlyAuthEnabled(true);
                builder.setOAuthConsumerKey(TWIT_CONS_KEY);
                builder.setOAuthConsumerSecret(TWIT_CONS_SEC_KEY);
                builder.setOAuth2TokenType(token.getTokenType());
                builder.setOAuth2AccessToken(token.getAccessToken());

                Twitter twitter = new TwitterFactory(builder.build()).getInstance();

                Query query = new Query(params[0]);
                query.setCount(50);
                QueryResult result;
                result = twitter.search(query);
                List<twitter4j.Status> tweets = result.getTweets();
                StringBuilder str = new StringBuilder();
                if (tweets != null) {
                    this.tweets = new ArrayList<Tweet>();
                    for (twitter4j.Status tweet : tweets) {
                        str.append("@" + tweet.getUser().getScreenName() + " - " + tweet.getText() + "\n");
                        System.out.println(str);
                        this.tweets.add(new Tweet("@" + tweet.getUser().getScreenName(), tweet.getText(), tweet.getUser().getName()));
                    }
                    return SUCCESS;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return FAILURE;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            progress.setVisibility(View.GONE);
            if (result == SUCCESS) {
                if (tweets.size() == 0) {
                    Toast.makeText(MainActivity.this, R.string.no_data, Toast.LENGTH_SHORT).show();
                    list.setVisibility(View.GONE);
                } else {
                    list.setAdapter(new TweetAdapter(MainActivity.this, tweets));
                    list.setVisibility(View.VISIBLE);
                }
            } else {
                list.setVisibility(View.GONE);
                Toast.makeText(MainActivity.this, getString(R.string.error), Toast.LENGTH_LONG).show();
            }
        }
    }
}

