package app.vegfrutweets.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import app.vegfrutweets.R;

public class SplashActivity extends AppCompatActivity {
    Animation zoom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        zoom = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.zoom_out);
        findViewById(R.id.logo).startAnimation(zoom);
        displayScreen();
    }

    private void displayScreen() {
        Thread background = new Thread() {
            public void run() {
                try {
                    sleep(1500);
                    Intent splashIntent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(splashIntent);
                    finish();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        background.start();
    }
}
