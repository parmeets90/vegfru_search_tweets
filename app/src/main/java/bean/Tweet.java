package bean;

public class Tweet {
    String tweetBy;
    String tweet;
    String tweetByName;

    public Tweet(String tweetBy, String tweet, String tweetByName) {
        this.tweetBy = tweetBy;
        this.tweet = tweet;
        this.tweetByName = tweetByName;
    }

    public String getTweetBy() {
        return tweetBy;
    }

    public void setTweetBy(String tweetBy) {
        this.tweetBy = tweetBy;
    }

    public String getTweet() {
        return tweet;
    }

    public void setTweet(String tweet) {
        this.tweet = tweet;
    }

    public String getTweetByName() {
        return tweetByName;
    }

    public void setTweetByName(String tweetByName) {
        this.tweetByName = tweetByName;
    }

}
